﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EduMark.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace PetClinic.Controllers
{
    public class ContactController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ContactController(ApplicationDbContext db)
        {
            _db = db;
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}